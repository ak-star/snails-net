# ProgressManager

#### 介绍
ProgressManager — 文件上传下载进度

#### 软件架构
设计模式：单利模式、责任链模式



#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

> 可参考JessYanCoding的[ProgressManager](https://github.com/JessYanCoding/ProgressManager/blob/master/README-zh.md)；初始化部分已经集成到```RetrofitClient```中，无需另行初始化。

*    **ProgressManager  文件上传下载进度**

1.   **使用方法**

```

// Glide 下载监听
ProgressManager.getInstance().addResponseListener(IMAGE_URL, getGlideListener());


// Okhttp/Retofit 下载监听
ProgressManager.getInstance().addResponseListener(DOWNLOAD_URL, getDownloadListener());


// Okhttp/Retofit 上传监听
ProgressManager.getInstance().addRequestListener(UPLOAD_URL, getUploadListener());

```

* [框架的构思和实现可以看这篇文章](https://juejin.im/post/593d85e55c497d006b90433d)

* 效果图

![效果图](https://images.gitee.com/uploads/images/2019/0412/165423_1d8dce79_2666596.gif "progressManager.gif")

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)