# RestClient

#### 介绍
RestClient — retrofit网络请求（RetrofitClient进一步封装）

* 需要权限:

```

<!-- ============================= 敏感权限 ============================= -->
<!-- 访问电话状态 -->
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<!-- 允许程序读取外部存储文件 -->
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
<!-- 允许程序写入外部存储，如SD卡上写文件 -->
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

<!-- ============================= 一般权限 ============================= -->
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

```

#### 软件架构
构建模式：builder模式

**RestClient是retrofit网络请求（RetrofitClient进一步封装）**

#### 使用说明

```
✔️ 缓存和重连机制，都是在网络请求头中追加相应键值对来完成设置


缓存机制：
键 - "cache-method"
值 - 【"only_net" - 只使用网络】、【"net_or_cache" - 优先请求网络，无网时使用缓存】


重连机制：
键 - "retry-count"
值 - 任意正整数，例如："retry-count"=2

```

> 使用案例，多baseUrl使用

```

import android.app.Application;
import android.os.Handler;

import com.ak.aigo.BuildConfig;
import com.ak.aigo.configuration.BaseUrl;
import com.ak.aigo.configuration.Configuration;
import com.ak.aigo.test.TestJson;
import com.snails.app.SnailsApp;
import com.snails.app.configuration.IConfiguration;
import com.snails.app.configuration.model.SnailsDebug;
import com.snails.app.configuration.model.SnailsNetwork;
import com.snails.net.restful.common.RestCreator;
import com.snails.net.restful.configuration.model.SnailsRestful;
import com.snails.net.retrofit.interceptor.HeaderInterceptor;

import okhttp3.Interceptor;

import static com.ak.aigo.configuration.BaseUrl.COOK;

public class AiGoApplication extends Application {

    private IConfiguration iConfiguration = new IConfiguration() {

        @Override
        public SnailsRestful restful() {
            return new SnailsRestful()
                    .timeOut(15).wTimeOut(20).rTimeOut(20)
                    .debug(BuildConfig.DEBUG, "爱购")
                    .supportMoreBaseUrl(true)           // 支持多BaseUrl
                    .addInterceptor(commonHeaders())
                    .addInterceptors(new TestJson().get());
        }

        @Override
        public SnailsDebug debug() {
            return new SnailsDebug().tag("Snails").debug(true);
        }

        @Override
        public SnailsNetwork network() {
            return new SnailsNetwork().isOnline(Configuration.online)
                    .testUrl(BaseUrl.testBaseUrl1).productUrl(BaseUrl.productBaseUrl1);
        }
    };

    // 网络请求，请求头公共参数
    private Interceptor commonHeaders() {
        // 添加公共参数拦截器
        return new HeaderInterceptor.Builder().addHeaderParams("paltform","android").build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SnailsApp.instance().init(this, iConfiguration);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                // 添加额外支持多BaseUrl
                RestCreator.putDomain(COOK, Configuration.baseUrl2);
            }
        });
    }

}

```

```

public void onClick(View v) {
    new RestClient.Builder().url("cook/category")
            .headers(RetrofitClient.DOMAIN, BaseUrl.COOK)   // 设置当前接口的baseUrl(切换baseUrl)
            .params("key", BaseUrl.COOK)
            .prepare(new IPrepare() {
                @Override
                public void onPrepare() {
                    if (dialog == null)
                        dialog = new LoadingDialog.Builder().build(ctx);
                    dialog.show();
                }
            })
            .succeed(new ISucceed() {
                @Override
                public void onSucceed(final String response) { }
            })
            .completed(new ICompleted() {
                @Override
                public void onCompleted() {
                    if (dialog != null && dialog.isShowing()) { dialog.dismiss(); }
                }
            })
            .build().get();
}

```


> GET请求

```

new RestClient.Builder()
    .url("data/cityinfo/101010100.html")
    .headers(RetrofitConstant.CACHE_METHOD, RetrofitConstant.NET_OR_CACHE)
    .prepare(new IPrepare() {
        @Override
        public void onPrepare() {
            if (dialog == null)
                dialog = new LoadingDialog.Builder().build(ctx);
            dialog.show();
        }
    })
    .succeed(new ISucceed() {
        @Override
        public void onSucceed(String response) { Logger.json(response); }
    })
    .completed(new ICompleted() {
        @Override
        public void onCompleted() {
            if (dialog != null && dialog.isShowing()) { dialog.dismiss(); }
        }
    })
    .build().get();

```

* 运行结果

![输入图片说明](https://gitee.com/uploads/images/2019/0411/135842_59c91feb_2666596.png "12.png")


---------------------------

> download文件，含有下载进度

```

private final String baseUrl = "http://image.fvideo.cn";
private final String url = "/uploadfile/2014/12/07/img11419320057966.jpg";

public void onClick(View v) {
    new RestClient.Builder().url(url)
            .downloadFile("down-zip", "img11419320057966.jpg")
            .prepare(new IPrepare() {
                @Override
                public void onPrepare() {
                    if (dialog == null)
                        dialog = new LoadingDialog.Builder().build(ctx);
                    dialog.show();
                }
            })
            .succeed(new ISucceed() {
                @Override
                public void onSucceed(String response) {
                    Logger.json(response);
                }
            })
            .completed(new ICompleted() {
                @Override
                public void onCompleted() {
                    if (dialog != null && dialog.isShowing()) { dialog.dismiss(); }
                }
            })
            .build().download();

    ProgressManager.getInstance().addResponseListener(baseUrl + url, new ProgressListener() {
        @Override
        public void onProgress(ProgressInfo progressInfo) {
            if (dialog != null) {
                dialog.setMessage("" + progressInfo.getPercent() + "%");
            }
        }

        @Override
        public void onError(long id, Exception e) { }
    });
}

```

> 运行结果

* 下载中

![下载中](https://images.gitee.com/uploads/images/2019/0412/205532_ad97c945_2666596.png "图片11.png")

* 下载完成

![下载完成](https://images.gitee.com/uploads/images/2019/0412/205544_b5059b19_2666596.png "图片22.png")


---------------------------

2.   **方法介绍**

* RestClient.Builder

```

url(String url): Builder        设置网络请求url


params(Map<String, Object> params): Builder     设置post/get请求参数
params(String key, Object value): Builder       设置post/get请求参数


headers(Map<String, String> headers): Builder   设置请求头参数
headers(String key, String value): Builder      设置请求头参数
headers(String key, Object value): Builder      设置请求头参数


file(File file): Builder            设置上传文件路径
file(String file): Builder          设置上传文件路径


downloadFile(String dir, String fileName): Builder      下载文件地址，文件夹(可null)，文件名(不可为null)


raw(String raw): Builder        设置post/put请求原始数据（如：上传json数据）


succeed(ISucceed succeed): Builder          请求成功回调数据
error(IError error): Builder                请求错误回调数据
failed(IFailed failed): Builder             请求失败回调数据
prepare(IPrepare prepare): Builder          请求前准备工作
completed(ICompleted completed): Builder    请求完成后结束工作


build(): RestClient             构建RestClient对象

```

* RestClient

```

get(): void               GET请求

post(): void              POST请求

put(): void               PUT请求

delete(): void            DELETE请求

upload(): void            上传文件

download(): void          下载文件

```

* RestCreator

```

putDomain(String name, String baseUrl): void    可在 App 运行时,随时切换 BaseUrl (指定了 Domain-Name header 的接口)

```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)