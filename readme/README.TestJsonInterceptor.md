# TestJsonInterceptor

#### 介绍
TestJsonInterceptor — 自定义Json模拟网络请求，进行快速开发

#### 软件架构

#### 使用说明

1.   **TestJsonInterceptor 自定义Json模拟网络请求，进行快速开发**

* 存储路径```res => raw```

2.   **使用案例**

* application

```

import android.app.Application;

import com.ak.aigo.BuildConfig;
import com.ak.aigo.R;
import com.snails.app.SnailsApp;
import com.snails.app.configuration.IConfiguration;
import com.snails.app.configuration.model.SnailsDebug;
import com.snails.app.configuration.model.SnailsNetwork;
import com.snails.net.restful.configuration.model.SnailsRestful;
import com.snails.net.retrofit.interceptor.HeaderInterceptor;
import com.snails.net.retrofit.interceptor.TestJsonInterceptor;

import okhttp3.Interceptor;

public class AiGoApplication extends Application {

    private IConfiguration iConfiguration = new IConfiguration() {
        // 是否正式上线，默认 true-正式上线
        private final boolean online = false;
        // 测试地址
        private final String testUrl = "http://www.weather.com.cn";
//        private final String testUrl = "http://image.fvideo.cn";
        // 正式地址
        private final String productUrl = "http://www.baidu.com";

        @Override
        public SnailsRestful restful() {
            return new SnailsRestful().timeOut(15).wTimeOut(20).rTimeOut(20)
                    .debug(BuildConfig.DEBUG, "爱购")
                    .addInterceptor(httpCommonInterceptor())
                    .addInterceptor(new TestJsonInterceptor(
                            "data/cityinfo/101010100.html", R.raw.test_json
                    ));
        }

        @Override
        public SnailsDebug debug() {
            return new SnailsDebug().tag("Snails").debug(true);
        }

        @Override
        public SnailsNetwork network() {
            return new SnailsNetwork().isOnline(online)
                    .testUrl(testUrl).productUrl(productUrl);
        }
    };

    /**
     * 网络请求，请求头公共参数
     * @return
     */
    private Interceptor httpCommonInterceptor() {
        // 添加公共参数拦截器
        return new HeaderInterceptor.Builder()
                .addHeaderParams("paltform","android")
                .addHeaderParams("userToken","zhys-fxy-gzr")
                .addHeaderParams("userId","00001")
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SnailsApp.instance().init(this, iConfiguration);
    }

}

```

* activity

```

private Context ctx = null;
private LoadingDialog dialog;

public void onClick(View v) {
    new RestClient.Builder()
            .url("data/cityinfo/101010100.html")
            .headers(RetrofitConstant.CACHE_METHOD, RetrofitConstant.ONLY_NET)
            .params("JSON", "return-content-type-json")
            .prepare(new IPrepare() {
                @Override
                public void onPrepare() {
                    if (dialog == null)
                        dialog = new LoadingDialog.Builder().build(ctx);
                    dialog.show();
                }
            })
            .succeed(new ISucceed() {
                @Override
                public void onSucceed(String response) {
                    Logger.json(response);
                }
            })
            .completed(new ICompleted() {
                @Override
                public void onCompleted() {
                    if (dialog != null && dialog.isShowing()) { dialog.dismiss(); }
                }
            })
            .build().get();
}

```

3.   **示例图**

> 图1

![图1](https://images.gitee.com/uploads/images/2019/0415/162130_7ff4e8aa_2666596.png "图片111111111.png")

> 图2

![图2](https://images.gitee.com/uploads/images/2019/0415/162151_32844e24_2666596.png "图片2222222.png")

> 图3

![图3](https://images.gitee.com/uploads/images/2019/0415/162208_617654c8_2666596.png "图片333333333.png")


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)