# RetrofitClient

#### 介绍
RetrofitClient — retrofit网络请求

* 需要权限:

```

<!-- ============================= 敏感权限 ============================= -->
<!-- 访问电话状态 -->
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<!-- 允许程序读取外部存储文件 -->
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
<!-- 允许程序写入外部存储，如SD卡上写文件 -->
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

<!-- ============================= 一般权限 ============================= -->
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

```

#### 软件架构
构建模式：builder模式

#### 使用说明

**RetrofitClient retrofit网络请求**，建议项目中只构建一个本类的实例

```
✔️ 缓存和重连机制，都是在网络请求头中追加相应键值对来完成设置


缓存机制：
键 - "cache-method"
值 - 【"only_net" - 只使用网络】、【"net_or_cache" - 优先请求网络，无网时使用缓存】


// 重连机制(暂时关闭, 开启okHttp重试机制)：
// 键 - "retry-count"
// 值 - 任意正整数，例如："retry-count"=2

```

-------------------------------

1.   **使用案例**   

* RestClient

```

import com.ak.aigo.BuildConfig;
import com.snails.app.SnailsApp;
import com.snails.net.retrofit.RetrofitClient;
import com.snails.net.retrofit.interceptor.HeaderInterceptor;

import okhttp3.Interceptor;

public final class RestClient {
    private static final int DEFAULT_TIME_OUT = 5;             // 超时时间 单位：秒
    private static final int DEFAULT_READ_WRITE_TIME_OUT = 10; // 读写超时时间 单位：秒

    private final RetrofitClient mRetrofitClient;

    private RestClient() {
        mRetrofitClient = new RetrofitClient.Builder()
                .timeOut(DEFAULT_TIME_OUT)
                .wTimeOut(DEFAULT_READ_WRITE_TIME_OUT)
                .rTimeOut(DEFAULT_READ_WRITE_TIME_OUT)
                .addInterceptor(httpCommonInterceptor())
                .debug(BuildConfig.DEBUG, "aiGo-net")
                .baseUrl("http://www.weather.com.cn")
                .build(SnailsApp.instance().getCtx());
    }

    /**
     * 网络请求，请求头公共参数
     * @return
     */
    private Interceptor httpCommonInterceptor() {
        // 添加公共参数拦截器
        return new HeaderInterceptor.Builder()
                .addHeaderParams("paltform","android")
                .addHeaderParams("userToken","zhys-fxy-gzr")
                .addHeaderParams("userId","00001")
                .build();
    }

    /**
     * 获取对应的Service
     *
     * @param service Service 的 class
     * @param <T>
     * @return
     */
    public <T> T create(Class<T> service) {
        return mRetrofitClient.create(service);
    }

    private static class RestClientHolder {
        private static final RestClient INSTANCE = new RestClient();
    }

    /**
     * 获取 RetrofitClient
     *
     * @return
     */
    public static RestClient getInstance() {
        return RestClientHolder.INSTANCE;
    }


}

```

* TestService

```

import com.ak.aigo.net.model.TestModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface TestService {
    @Headers({
            "cache-json: net_or_cache",
            "retry-count: 2"
    })
    @GET("data/cityinfo/101010100.html")
    Observable<TestModel> getWeather();
}


```

* TestModel

```

import java.io.Serializable;

public class TestModel implements Serializable {
    private Weatherinfo weatherinfo = null;

    public Weatherinfo getWeatherinfo() {
        return weatherinfo;
    }

    public void setWeatherinfo(Weatherinfo weatherinfo) {
        this.weatherinfo = weatherinfo;
    }

    @Override
    public String toString() {
        return "TestModel{" +
                "weatherinfo=" + weatherinfo +
                '}';
    }
}

```

```

private View.OnClickListener onClickListener = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        RestClient.getInstance()
                .create(TestService.class)
                .getWeather()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TestModel>() {
                    @Override
                    public void onSubscribe(Disposable disposable) { }

                    @Override
                    public void onNext(TestModel s) {
                        Logger.json(new Gson().toJson(s));
                    }

                    @Override
                    public void onError(Throwable throwable) { }

                    @Override
                    public void onComplete() { }
                });
    }
};

```

* 运行结果

![输入图片说明](https://gitee.com/uploads/images/2019/0411/135842_59c91feb_2666596.png "12.png")


---------------------------

2.   **方法介绍**

* RetrofitClient.Builder

```

timeOut(int time): Builder          连接超时时间【单位：秒】默认-5秒
rTimeOut(int time): Builder         读操作超时时间【单位：秒】默认-5秒
wTimeOut(int time): Builder         写操作超时时间【单位：秒】默认-5秒


baseUrl(String url): Builder                    设置根url
debug(boolean debug, String tag): Builder       日志拦截器。默认：debug-true启动，tag-"Snails-net"

supportMoreBaseUrl(boolean isSupport): Builder  是否支持多个baseUrl切换


// 缓存设置。
// 默认：大小5MiB，
//      路径 —— 有SD卡，应用目录下files->cache->JsonCache，
//              无SD卡，内存存储下应用目录下files->cache->JsonCache
cache(File file, long size): Builder


addInterceptor(Interceptor interceptor): Builder            添加拦截器，在response中调用
addNetworkInterceptor(Interceptor interceptor): Builder     添加网络-请求拦截器，在request和response中各调用一次


build(Context ctx): RetrofitClient      构建RetrofitClient对象

```

* RetrofitClient

```

putDomain(String name, String baseUrl): void    可在 App 运行时,随时切换 BaseUrl (指定了 Domain-Name header 的接口)

create(Class<T> service): T                     获取对应的Service

```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)