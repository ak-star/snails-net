# DiskLruCacheHelper

#### 介绍
DiskLruCacheHelper — 仿LruCache的磁盘文件管理工具

* 需要权限:

```

<!-- ============================= 敏感权限 ============================= -->
<!-- 允许程序读取外部存储文件 -->
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
<!-- 允许程序写入外部存储，如SD卡上写文件 -->
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

```

#### 软件架构
构建模式：builder模式

#### 使用说明

1.   **方法介绍**

* DiskLruCacheHelper.Builder

```

directory(File directory): Builder          创建存储文件目录
appVersion(int appVersion): Builder         当前存储版本，当版本变更时，会清除之前存储的数据
maxSize(long maxSize): Builder              本地存储最大占用空间



build(): DiskLruCacheHelper                             构建DiskLruCacheHelper对象，已设置directory

build(Context ctx): DiskLruCacheHelper                  构建DiskLruCacheHelper对象，使用默认directory

build(Context ctx, String dirName): DiskLruCacheHelper  构建DiskLruCacheHelper对象，在默认directory根目录下创建一个子目录

```

* DiskLruCacheHelper

```

putString(String key, String value): void       存储String类型数据
getString(String key): String                   获取String类型数据

putBytes(String key, byte[] bytes): void        存储二进制数据
getBytes(String key): byte[]                    获取二进制数据

putSerializable(String key, Serializable s): void   存储可序列化对象
<T> getSerializable(String key): T                  返序列化获取数据对象

putBitmap(String key, Bitmap bitmap): void      存储图片
getBitmap(String key): Bitmap                   获取图片



remove(String key): boolean         删除指定存储文件数据
delete(): void                      清除所有存储文件
size(): long                        当前已使用缓存大小
isClosed(): boolean                 当前DiskLruCache是否已关闭
close(): void                       关闭DiskLruCache，关闭后无法进行读写操作
setMaxSize(long maxSize): void      设置本地最大缓存大小
getMaxSize(): long                  得到当前最大缓存大小
getDirectory(): File                得到DiskLruCache本地缓存路径

```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)