# HeaderInterceptor

#### 介绍
HeaderInterceptor — 网络请求头，追加公共参数

#### 软件架构
构建模式：builder模式

#### 使用说明

1.   **HeaderInterceptor 网络请求头，追加公共参数**

```

/**
 * 网络请求，请求头公共参数
 * @return
 */
private Interceptor httpCommonInterceptor() {
    // 添加公共参数拦截器
    return new HeaderInterceptor.Builder()
            .addHeaderParams("paltform", "android")
            .addHeaderParams("userToken", "zhys_fxy_gzr")
            .addHeaderParams("userId", 00001)
            .build();
}

mRetrofitClient = new RetrofitClient.Builder()
        .timeOut(DEFAULT_TIME_OUT)
        .wTimeOut(DEFAULT_READ_WRITE_TIME_OUT)
        .rTimeOut(DEFAULT_READ_WRITE_TIME_OUT)
        .addInterceptor(httpCommonInterceptor())
        .debug(true, "Snails-net")
        .baseUrl(baseUrl())
        .build(appCtx);

```

【方法介绍】

* HeaderInterceptor.Builder

```

addHeaderParams(Map<String, String> maps): Builder      添加Map类型公共header信息
addHeaderParams(String key, String value): Builder      添加String类型公共header信息
addHeaderParams(String key, int value): Builder         添加int类型公共header信息
addHeaderParams(String key, float value): Builder       添加float类型公共header信息
addHeaderParams(String key, long value): Builder        添加long类型公共header信息
addHeaderParams(String key, double value): Builder      添加double类型公共header信息


build(): HeaderInterceptor      构建HeaderInterceptor对象

```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)