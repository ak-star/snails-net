# RxRestClient

#### 介绍
RxRestClient — retrofit+rxjava网络请求

* 需要权限:

```

<!-- ============================= 敏感权限 ============================= -->
<!-- 访问电话状态 -->
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<!-- 允许程序读取外部存储文件 -->
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
<!-- 允许程序写入外部存储，如SD卡上写文件 -->
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

<!-- ============================= 一般权限 ============================= -->
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

```

#### 软件架构
构建模式：builder模式

**RxRestClient是retrofit+rxjava的网络请求封装**

#### 使用说明

```
✔️ 缓存和重连机制，都是在网络请求头中追加相应键值对来完成设置


缓存机制：
键 - "cache-method"
值 - 【"only_net" - 只使用网络】、【"net_or_cache" - 优先请求网络，无网时使用缓存】


重连机制：
键 - "retry-count"
值 - 任意正整数，例如："retry-count"=2

```

> 使用案例

```

RxRestClient client = new RxRestClient.Builder()
        .url("data/cityinfo/101010100.html")
        .headers(RetrofitConstant.CACHE_METHOD, RetrofitConstant.NET_OR_CACHE)
        .build();

client.get().subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable disposable) { }
            @Override
            public void onNext(String s) { Logger.json(s); }
            @Override
            public void onError(Throwable throwable) { }
            @Override
            public void onComplete() { }
        });

```

* 运行结果

![输入图片说明](https://gitee.com/uploads/images/2019/0411/135842_59c91feb_2666596.png "12.png")


---------------------------

2.   **方法介绍**

* RxRestClient.Builder

```

url(String url): Builder        设置网络请求url


params(Map<String, Object> params): Builder     设置post/get请求参数
params(String key, Object value): Builder       设置post/get请求参数


headers(Map<String, String> headers): Builder   设置请求头参数
headers(String key, String value): Builder      设置请求头参数
headers(String key, Object value): Builder      设置请求头参数


file(File file): Builder            设置上传文件路径
file(String file): Builder          设置上传文件路径


raw(String raw): Builder        设置post/put请求原始数据（如：上传json数据）


build(): RxRestClient           构建RxRestClient对象

```

* RxRestClient

```

get(): Observable<String>               GET请求

post(): Observable<String>              POST请求

put(): Observable<String>               PUT请求

delete(): Observable<String>            DELETE请求

upload(): Observable<String>            上传文件

download(): Observable<String>          下载文件

```

* RestCreator

```

putDomain(String name, String baseUrl): void    可在 App 运行时,随时切换 BaseUrl (指定了 Domain-Name header 的接口)

```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)