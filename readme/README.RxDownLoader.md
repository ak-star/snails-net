# RxDownLoader

#### 介绍
RxDownLoader — rx文件下载辅助器（RxRestClient进一步封装）

* 需要权限:

```

<!-- ============================= 敏感权限 ============================= -->
<!-- 访问电话状态 -->
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<!-- 允许程序读取外部存储文件 -->
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
<!-- 允许程序写入外部存储，如SD卡上写文件 -->
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

<!-- ============================= 一般权限 ============================= -->
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

```

#### 软件架构
构建模式：builder模式

**RxDownLoader是rx文件下载辅助器（RxRestClient进一步封装）**

#### 使用说明

> 使用案例

```

private final String baseUrl = "http://image.fvideo.cn";
private final String url = "/uploadfile/2014/12/07/img11419320057966.jpg";

public void onClick(View v) {
    if (dialog == null)
        dialog = new LoadingDialog.Builder().build(ctx);
    dialog.show();

    new RxDownLoader.Builder().url(url)
            .headers("down-jpg", "JPG").params("from", "baidu")
            .downloadFile("down-zip", "img20190415.jpg")
            .observer(new Observer<File>() {
                @Override
                public void onSubscribe(Disposable d) { }
                @Override
                public void onNext(File file) {
                    final String message = file != null ? file.toString() : "空file";
                    Logger.i(message);
                }
                @Override
                public void onError(Throwable e) { }
                @Override
                public void onComplete() {
                    if (dialog != null && dialog.isShowing()) { dialog.dismiss(); }
                }
            })
            .build(ctx);

    ProgressManager.getInstance().addResponseListener(baseUrl + url, new ProgressListener() {
        @Override
        public void onProgress(ProgressInfo progressInfo) {
            if (dialog != null) {
                dialog.setMessage("" + progressInfo.getPercent() + "%");
            }
        }

        @Override
        public void onError(long id, Exception e) { }
    });
}

```

> 运行结果

* 下载中

![下载中](https://images.gitee.com/uploads/images/2019/0412/205532_ad97c945_2666596.png "图片11.png")

* 下载完成

![下载完成](https://images.gitee.com/uploads/images/2019/0412/205544_b5059b19_2666596.png "图片22.png")


---------------------------

2.   **方法介绍**

* RxDownLoader.Builder

```

url(String url): Builder        设置网络请求url


params(Map<String, Object> params): Builder     设置download请求参数
params(String key, Object value): Builder       设置download请求参数


headers(Map<String, String> headers): Builder   设置请求头参数
headers(String key, String value): Builder      设置请求头参数
headers(String key, Object value): Builder      设置请求头参数


downloadFile(String dir, String fileName): Builder      下载文件地址，文件夹(可null)，文件名(不可为null)


observer(Observer<File> observer): Builder      下载回调


build(): RxDownLoader             构建RxDownLoader对象

```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)