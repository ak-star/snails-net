# ApkUtil

#### 介绍
apk辅助工具 - ApkUtil


#### 软件架构


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1.   **ApkUtil apk辅助工具**

* 【方法说明】

```
/**
 * 安装apk
 * @param activity 安装apk的activity
 * @param file 安装apk的文件所在路径，(7.0文件访问权限)(8.0安装apk权限)
 * @param requestCode 8.0无安装权限时，申请权限返回申请权限结果的code
 */
public static void install(Activity activity, File file, int requestCode)

```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)