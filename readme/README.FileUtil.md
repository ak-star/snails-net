# FileUtil

#### 介绍
文件工具 - FileUtil

* 需要 危机权限:

```

<!-- 允许程序读取外部存储文件 -->
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
<!-- 允许程序写入外部存储，如SD卡上写文件 -->
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

```


#### 软件架构


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

* 【方法说明】

```

/**
 * @return true-SD卡可用，false-SD卡不可用
 */
public static boolean isSdCardExist()


/**
 * {@link android.Manifest.permission#WRITE_EXTERNAL_STORAGE}
 * {@link android.Manifest.permission#READ_EXTERNAL_STORAGE}
 *
 * @return 优先返回SD卡下，应用的cache文件夹；
 * SD卡不可用下，返回内存存储应用下的cache文件夹；
 */
public static File appCacheFolder(Context ctx)


/**
 * {@link android.Manifest.permission#WRITE_EXTERNAL_STORAGE}
 * {@link android.Manifest.permission#READ_EXTERNAL_STORAGE}
 *
 * @return 优先返回SD卡下，应用的files->cache文件夹；
 * SD卡不可用下，返回内存存储应用下的files->cache文件夹；
 */
public static File appCreateFolder(Context ctx, String folderName)


/**
 * {@link android.Manifest.permission#WRITE_EXTERNAL_STORAGE}
 * {@link android.Manifest.permission#READ_EXTERNAL_STORAGE}
 *
 * @return 优先返回SD卡下，应用的files->cache文件夹；
 * SD卡不可用下，返回内存存储应用下的files->cache文件夹；
 */
public static File appCreateFile(Context ctx, String folderName, String fileName)


/**
 * {@link android.Manifest.permission#WRITE_EXTERNAL_STORAGE}
 * {@link android.Manifest.permission#READ_EXTERNAL_STORAGE}
 *
 * @return 优先返回SD卡下，应用的cache文件夹路径；
 * SD卡不可用下，返回内存存储应用下的cache文件夹路径；
 */
public static String appCachePath(Context ctx)


/**
 * {@link android.Manifest.permission#WRITE_EXTERNAL_STORAGE}
 * {@link android.Manifest.permission#READ_EXTERNAL_STORAGE}
 *
 * @return 优先返回SD卡下，应用的files文件夹；
 * SD卡不可用下，返回内存存储应用下的files文件夹；
 */
public static File appFilesFolder(Context ctx)


/**
 * {@link android.Manifest.permission#WRITE_EXTERNAL_STORAGE}
 * {@link android.Manifest.permission#READ_EXTERNAL_STORAGE}
 *
 * @return 优先返回SD卡下，应用的files文件夹路径；
 * SD卡不可用下，返回内存存储应用下的files文件夹路径；
 */
public static String appFilesPath(Context ctx)


/**
 * {@link android.Manifest.permission#WRITE_EXTERNAL_STORAGE}
 * {@link android.Manifest.permission#READ_EXTERNAL_STORAGE}
 *
 * @param type {@code null}
 *             {@link Environment#DIRECTORY_MUSIC},
 *             {@link Environment#DIRECTORY_PODCASTS},
 *             {@link Environment#DIRECTORY_RINGTONES},
 *             {@link Environment#DIRECTORY_ALARMS},
 *             {@link Environment#DIRECTORY_NOTIFICATIONS},
 *             {@link Environment#DIRECTORY_PICTURES}, or
 *             {@link Environment#DIRECTORY_MOVIES}.
 * @return 优先返回SD卡下，应用的files文件夹路径；
 * SD卡不可用下，返回内存存储应用下的files文件夹路径；
 */
public static File appSDCardFilesFolder(Context ctx, @Nullable String type)


/**
 * {@link android.Manifest.permission#WRITE_EXTERNAL_STORAGE}
 * {@link android.Manifest.permission#READ_EXTERNAL_STORAGE}
 *
 * @param type {@code null}
 *             {@link Environment#DIRECTORY_MUSIC},
 *             {@link Environment#DIRECTORY_PODCASTS},
 *             {@link Environment#DIRECTORY_RINGTONES},
 *             {@link Environment#DIRECTORY_ALARMS},
 *             {@link Environment#DIRECTORY_NOTIFICATIONS},
 *             {@link Environment#DIRECTORY_PICTURES}, or
 *             {@link Environment#DIRECTORY_MOVIES}.
 * @return 优先返回SD卡下，应用的files文件夹路径；
 * SD卡不可用下，返回内存存储应用下的files文件夹路径；
 */
public static String appSDCardFilesPath(Context ctx, @Nullable String type)


/**
 * @return 返回文件夹 {@link File}
 */
@Nullable
public static File getFolder(String folderPath)


/**
 * @return 返回文件 {@link File}
 */
@Nullable
public static File getFile(String filePath)


/**
 * 判断文件是否存在
 */
public static boolean isFileExists(final File file)


/**
 * 递归删除文件、文件夹下的所有数据
 */
public static void deleteFile(File file)


/**
 * 递归删除文件、文件夹下的所有数据
 */
public static void deleteFile(String filePath)


/**
 * 往磁盘中写入文件
 */
public static File writeToDisk(InputStream is, Context ctx, String dir, String name)


/**
 * 往磁盘中写入文件
 */
public static File writeToDisk(InputStream is, File file)


/**
 * @return 返回文件、文件夹的大小
 */
public static long getFolderSize(File file)


/**
 * @return 返回文件、文件夹的大小
 */
public static long getFolderSize(String filePath)


/**
 * @return 返回文件、文件夹的大小
 */
public static String formatFileSize(File file)


/**
 * @return 返回文件、文件夹的大小
 */
public static String formatFileSize(String file)


/**
 * 读取raw目录中的文件,并返回为字符串
 */
public static String getRawFile(Context ctx, @RawRes int rawId)


/**
 * 读取assets目录下的文件,并返回字符串
 */
public static String getAssetsFile(Context ctx, String fileName)


/**
 * @return 格式化大小，返回{B、KB、MB、GB、TB}
 */
public static String formatSize(long size)

```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)