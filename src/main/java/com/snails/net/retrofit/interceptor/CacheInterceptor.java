package com.snails.net.retrofit.interceptor;

import android.text.TextUtils;

import com.snails.net.retrofit.interceptor.RetrofitConstant;
import com.snails.net.utils.NetworkUtil;

import java.io.IOException;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author lawrence
 * @link http://www.cnblogs.com/whoislcj/p/5537640.html
 * <p/>
 * final CacheControl.Builder builder = new CacheControl.Builder();
 * ----- builder.noCache(); // 不使用缓存，全部走网络
 * ----- builder.noStore(); // 不使用缓存，也不存储缓存
 * ----- builder.onlyIfCached(); // 只使用缓存
 * ----- builder.noTransform();  // 禁止转码
 * ----- builder.maxAge(10, TimeUnit.MILLISECONDS); // 指示客户机可以接收生存期不大于指定时间的响应。
 * ----- builder.maxStale(10, TimeUnit.SECONDS);    // 指示客户机可以接收超出超时期间的响应消息
 * ----- builder.minFresh(10, TimeUnit.SECONDS);    // 指示客户机可以接收响应时间小于当前时间加上指定时间的响应。
 * ----- CacheControl cache = builder.build();      // cacheControl
 * <p/>
 * CacheControl 常量
 * ----- CacheControl.FORCE_CACHE;   // 仅仅使用缓存
 * ----- CacheControl.FORCE_NETWORK; // 仅仅使用网络
 */
public class CacheInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        // 得到缓存使用策略
        final String cacheJson = request.header(RetrofitConstant.CACHE_METHOD);
        if (!TextUtils.isEmpty(cacheJson)) {
            // 无网络，且缓存使用策略为：NET_OR_CACHE【即：先网络后缓存】
            if (!NetworkUtil.instance().isConnected()
                    && RetrofitConstant.NET_OR_CACHE.equals(cacheJson)) {
                request = request.newBuilder().cacheControl(CacheControl.FORCE_CACHE).build();
            }

            if (!RetrofitConstant.NET_OR_CACHE.equals(cacheJson)) {
                // 不需要存储缓存
                return chain.proceed(request);
            } else if (NetworkUtil.instance().isConnected()) {
                // 需要存储缓存，且网络正常
                Response originalResponse = chain.proceed(request);
                String cacheControl = request.cacheControl().toString();
                return originalResponse.newBuilder()
                        .header("Cache-Control", cacheControl)
                        .removeHeader("Pragma") // 清除头信息，因为服务器如果不支持，会返回一些干扰信息，不清除下面无法生效
                        .build();
            } else {
                // 需要存储缓存，且无网络时 设置超时为1周
                Response originalResponse = chain.proceed(request);
                int maxStale = 60 * 60 * 24 * 7;
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                        .removeHeader("Pragma")
                        .build();
            }
        }

        return chain.proceed(request);
    }

}
