package com.snails.net.retrofit.interceptor;

public final class RetrofitConstant {

    private RetrofitConstant() {}

    // ================================================================================
    // --- ---
    // ================================================================================
    /**
     * 重试次数
     */
    public static final String RETRY_COUNT = "retry-count";

    // ================================================================================
    // --- ---
    // ================================================================================
    /**
     * 单个日志是否打印
     */
    public static final String LOGGER = "logger";

    public static final String TRUE = "true";

    public static final String FALSE = "false";

    // ================================================================================
    // --- ---
    // ================================================================================
    /**
     * 使用缓存方案 key
     */
    public static final String CACHE_METHOD = "cache-method";

    /**
     * 只使用网络 value
     */
    public static final String ONLY_NET = "only_net";

    /**
     * 优先请求网络，无网时使用缓存 value
     */
    public static final String NET_OR_CACHE = "net_or_cache";


}
