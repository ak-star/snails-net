package com.snails.net.restful.callback;

/**
 * 网络请求开始前调用
 * @author lawrence
 * @date 2019-04-12 11:56
 */
public interface IPrepare {

    void onPrepare();
}
