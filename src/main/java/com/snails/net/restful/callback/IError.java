package com.snails.net.restful.callback;

/**
 * 网络请求异常
 * @author lawrence
 * @date 2019-04-12 11:52
 */
public interface IError {

    void onError(int code, String msg);
}
