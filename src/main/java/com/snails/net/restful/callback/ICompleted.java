package com.snails.net.restful.callback;

/**
 * 网络请求完成后调用
 * @author lawrence
 * @date 2019-04-12 11:57
 */
public interface ICompleted {

    void onCompleted();
}
