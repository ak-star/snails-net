package com.snails.net.restful.callback;

/**
 * 网络请求失败回调
 * @author lawrence
 * @date 2019-04-12 11:50
 */
public interface IFailed {

    void onFailed();
}
