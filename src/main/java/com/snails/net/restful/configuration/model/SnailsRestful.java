package com.snails.net.restful.configuration.model;

import com.snails.net.SnailsNet;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;

public final class SnailsRestful {
    // ================================================================================
    // --- ---
    // ================================================================================
    // 超时设置
    public int default_time_out = 5;
    public int default_r_time_out = 5;
    public int default_w_time_out = 5;

    // 缓存设置
    public long cache_size = 5 * 1024 * 1024; // 5 MiB
    public File cache_file = null;  // 缓存路径

    // 日志
    public boolean debug = true;
    public String loggerTag = "Snails-net";

    // 支持多不同url的host
    public boolean more_base_url = false;

    // 拦截器设置
    private List<Interceptor> interceptors = null;
    private List<Interceptor> networkInterceptors = null;

    // ================================================================================
    // --- ---
    // ================================================================================
    /**
     * 连接超时时间，单位：秒
     */
    public SnailsRestful timeOut(int time) {
        default_time_out = time;
        return this;
    }

    /**
     * 读操作超时时间，单位：秒
     */
    public SnailsRestful rTimeOut(int time) {
        default_r_time_out = time;
        return this;
    }

    /**
     * 写操作超时时间，单位：秒
     */
    public SnailsRestful wTimeOut(int time) {
        default_w_time_out = time;
        return this;
    }

    /**
     * 缓存设置
     *
     * @param file 缓存存放路径
     * @param size 缓存大小
     * @return
     */
    public SnailsRestful cache(File file, long size) {
        cache_file = file;
        cache_size = size;
        return this;
    }

    /**
     * 日志输出
     *
     * @param debug true-输出,false-不输出
     * @param tag   标签
     * @return
     */
    public SnailsRestful debug(boolean debug, String tag) {
        this.debug = debug;
        this.loggerTag = tag;
        return this;
    }

    /**
     * 是否支持多个baseUrl切换
     */
    public SnailsRestful supportMoreBaseUrl(boolean isSupport) {
        this.more_base_url = isSupport;
        return this;
    }

    /**
     * 添加拦截器，在response中调用
     *
     * @param interceptor 拦截器
     * @return
     */
    public SnailsRestful addInterceptor(Interceptor interceptor) {
        if (interceptor != null) {
            if (interceptors == null) {
                interceptors = new ArrayList<>();
            }
            interceptors.add(interceptor);
        }
        return this;
    }

    /**
     * 添加拦截器，在response中调用
     */
    public SnailsRestful addInterceptors(List<Interceptor> interceptors) {
        if (interceptors != null && interceptors.size() > 0) {
            if (this.interceptors == null) {
                this.interceptors = new ArrayList<>();
            }
            this.interceptors.addAll(interceptors);
        }
        return this;
    }

    /**
     * 添加网络-请求拦截器，在request和response中各调用一次
     *
     * @param interceptor 拦截器
     * @return
     */
    public SnailsRestful addNetworkInterceptor(Interceptor interceptor) {
        if (interceptor != null) {
            if (networkInterceptors == null) {
                networkInterceptors = new ArrayList<>();
            }
            networkInterceptors.add(interceptor);
        }
        return this;
    }

    /**
     * 添加网络-请求拦截器，在request和response中各调用一次
     */
    public SnailsRestful addNetworkInterceptors(List<Interceptor> interceptors) {
        if (interceptors != null && interceptors.size() > 0) {
            if (this.networkInterceptors == null) {
                this.networkInterceptors = new ArrayList<>();
            }
            this.networkInterceptors.addAll(interceptors);
        }
        return this;
    }

    // ================================================================================
    // --- ---
    // ================================================================================
    public String baseUrl() {
        return SnailsNet.instance().baseUrl();
    }

    public List<Interceptor> interceptors() {
        return this.interceptors;
    }

    public List<Interceptor> networkInterceptors() {
        return this.networkInterceptors;
    }

}
