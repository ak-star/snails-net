package com.snails.net.restful.configuration;

import com.snails.net.restful.configuration.model.SnailsRestful;

public interface IConfiguration {
    /**
     * restful配置
     */
    SnailsRestful restful();
}
