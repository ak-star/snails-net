# snails-net

#### 介绍
安卓 — 网络库

✔️ 1、get\post

✔️ 2、缓存

✔️ 3、文件上传下载


#### 软件架构
软件架构说明


#### 安装教程
1. xxxx
2. xxxx
3. xxxx

#### 使用说明

> **共享目录请命名为**```down_loads```

* 1、初始化```SnailsNet```

```

private IConfiguration iConfiguration = new IConfiguration() {
    @Override
    public SnailsRestful restful() {
        return new SnailsRestful()
                .cache(new File("cache/json"), 5 * 1024 * 1024)
                .debug(true, "Snails-restful");
    }
};

private String baseUrl = "http://www.baidu.com";

SnailsNet.instance().init(this, baseUrl, iConfiguration);

```

#### 组件名称、及功能说明

名称|描述|使用方法
---|:--|:---
NetworkChangedManager|网络变化管理监听器|[NetworkChangedManager.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.NetworkChangedManager.md)
NetworkUtil|网络信息工具|[NetworkUtil.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.NetworkUtil.md)
FileUtil|文件工具|[FileUtil.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.FileUtil.md)
RetrofitClient|retrofit网络请求|[RetrofitClient.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.RetrofitClient.md)
HeaderInterceptor|网络请求头，追加公共参数|[HeaderInterceptor.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.HeaderInterceptor.md)
RxRestClient|retrofit+rxjava网络请求（RetrofitClient+rxjava封装）|[RxRestClient.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.RxRestClient.md)
RestClient|retrofit网络请求（RetrofitClient封装）|[RestClient.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.RestClient.md)
ProgressManager|文件上传下载进度|[ProgressManager.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.ProgressManager.md)
RxDownLoader|rx文件下载辅助器|[RxDownLoader.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.RxDownLoader.md)
TestJsonInterceptor|自定义Json模拟网络请求返回结果|[TestJsonInterceptor.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.TestJsonInterceptor.md)
DiskLruCacheHelper|仿LruCache的磁盘文件存储工具|[DiskLruCacheHelper.md](https://gitee.com/ak-star/snails-net/blob/master/readme/README.DiskLruCacheHelper.md)


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)